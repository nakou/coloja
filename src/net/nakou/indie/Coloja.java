/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.nakou.indie;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.graphics.FPSLogger;
import net.nakou.indie.Screens.GameScreen;
import net.nakou.indie.Screens.Menu;
import net.nakou.indie.Screens.ScreenManager;

/**
 *
 * @author Nakou
 */
public class Coloja extends Game{

    private ScreenManager sm = new ScreenManager(this);
    
    @Override
    public void create() {
	System.out.println("it Start!");
        Menu s1 = new Menu(this, "Menu");
        GameScreen s2 = new GameScreen(this,"Game");
        sm.addState(s1);
        sm.addState(s2);
        //sm.getState("Menu");
        this.setScreen( sm.getState("Menu") );
        //this.setScreen(s1);
    }
    
    /**
     * @param args the command line arguments
     */
    public static final String LOG = Coloja.class.getSimpleName();
    // a libgdx helper class that logs the current FPS each second
    private FPSLogger fpsLogger;
    
    public static void main(String[] args) {
 
	Game g = new Coloja();
        // define the window's title
        String title = "Coloja 0.1";
 
        // define the window's size
        int width = 800, height = 480;
 
        // whether to use OpenGL ES 2.0
        boolean useOpenGLES2 = false;
 
	
        // create the game
        new LwjglApplication( g, title, width, height, useOpenGLES2 );
    }


}
