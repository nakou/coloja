/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.nakou.indie.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import net.nakou.indie.Coloja;

/**
 *
 * @author Nakou
 */
public class Menu extends AbstractScreen{

    private Texture penguin;
    private OrthographicCamera camera;
    private SpriteBatch batch;
    private float x = 10;
    private float y = 10;    
    
    public Menu(Game g,String s){
	super(g,s);
	Gdx.app.log( Coloja.LOG, "Start Menu" );
	//penguin = new Texture(Gdx.files.internal("sprites/penguin3.png"));
	//background = new Texture(Gdx.files.internal("background/nuages2.png"));
	//batch = new SpriteBatch();
	//camera = new OrthographicCamera();
	//camera.setToOrtho(false, 800, 480);
    }
    
    @Override
    public void render(float f){
        // output the current FPS
	Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
	//camera.update();
	
	//batch.begin();
	//batch.setProjectionMatrix(camera.combined);
	//batch.draw(background, 0, 0, 800, 480);
	//batch.draw(penguin, x, y, 16, 32 );
	//batch.end();
	/*if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
	    x = x - 100 * Gdx.graphics.getDeltaTime();
	} else if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
	    x = x + 100 * Gdx.graphics.getDeltaTime();
	} else if(Gdx.input.isKeyPressed(Input.Keys.ENTER)) {
	    this.pause();
	}*/
    }
 
    @Override
    public void pause(){
        Gdx.app.log( Coloja.LOG, "Pausing game" );
	
	/*if(Gdx.input.isKeyPressed(Input.Keys.ENTER))
	    this.resume();*/
    }    
    
    @Override
    public void resize(int i, int i1) {
    }

    @Override
    public void show() {
	this.resume();
    }

    @Override
    public void hide() {
	this.pause();
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }
    
}
