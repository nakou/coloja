/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.nakou.indie.Screens;

import com.badlogic.gdx.Game;
import java.util.ArrayList;

/**
 *
 * @author abo
 */
public class ScreenManager {
    private AbstractScreen activeState;
    private ArrayList<AbstractScreen> screens;
    private Game g;
    
    public ScreenManager(Game g){
        this.g = g;
        screens = new ArrayList<>();
    }
    
    public void addState(AbstractScreen s){
	screens.add(s);
    }

    public AbstractScreen getState(String n){
	for(AbstractScreen s : screens){
	    if(s.getName().equals(n))
		return s;
	}
	return null;
    }
    
    public AbstractScreen getActive(){
        for(AbstractScreen s : screens){
            if(s.isActive()){
                return s;
            }
        }
        return null;
    }
}
