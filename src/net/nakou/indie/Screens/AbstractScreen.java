/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.nakou.indie.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;

/**
 *
 * @author Nakou
 */
public abstract class AbstractScreen implements Screen{

    protected Game g;
    protected boolean active;
    protected String name;
    
    
    public boolean isActive() {
        return active;
    }

    public String getName() {
        return this.name;
    }
    /**
     *
     */
    public AbstractScreen(Game g, String n){
	this.g = g;
        this.name = n;
    }
    
    @Override
    public abstract void render(float f);

    @Override
    public  abstract void resize(int i, int i1);

    @Override
    public  abstract void show();

    @Override
    public abstract void hide();

    @Override
    public void pause(){
        this.active = false;
    }

    @Override
    public void resume(){
        this.active = true;
    }

    @Override
    public abstract void dispose();    
}

